Unter dem Titel „BITS Behörden-IT-Sicherheitstraining“ hat im Jahr 2006 eine 
Arbeitsgruppe des Arbeitskreises Informationstechnologie des Städte- und 
Gemeindebundes Nordrhein-Westfalen das für Unternehmen konzipierte 
Computersicherheitstraining „open beware!“ an die Anforderungen von 
Verwaltungen angepasst. Mittlerweile liegt die aktualisierte Version 5 vor. 
Seit Oktober 2010 wird BITS von der Kommunal Agentur NRW GmbH
mit Unterstützung von Dr. Lutz Gollan, Behörde für Inneres und Sport, Hamburg, 
herausgegeben.

„BITS Behörden-IT-Sicherheitstraining“ basiert auf open beware!, das von der 
BDG GmbH & Co. KG, jetzt NTT Security (Germany) GmbH, herausgegeben wurde.

Die Urheber sind Herr Dr. Lutz Gollan und Herr Hartmut Honermann, PureSec GmbH.

Die technische Realisierung erfolgt durch Herrn Werner Eising, Stadt Coesfeld.

BITS ist kostenlos und steht unter der Creative Commons (CC) Lizenz BY-SA.

Das integrierte Bootstrap-Framework steht unter der MIT-Lizenz, bei einer Veränderung
von BITS etc. dürfen die Lizenzhinweise nicht aus dem Quelltext entfernt werden. 
Das Copyright für Bootstrap liegt bei Twitter.

Die Grafiken sind unterliegen der Common Criteria CC0 udn stammen von OpenClipart und 
Pixabay. Die Grafik viren03b-bot.jpg wurde von Herrn Jörg Lekschas erstellt.

Beim Kapitel "Cloud" hat Frau Heike Brzezina wertvolle Hinweise gegeben.

Weitere Information in der Datei readme.txt
